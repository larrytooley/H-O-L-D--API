from django.test import TestCase, Client
from django.template.loader import render_to_string

# Create your tests here.

class TestIndexView(TestCase):

    def test_index_view(self):
        client = Client()
        response = client.get('/')
        self.assertEqual(response.status_code, 200)

        with self.assertTemplateUsed('index.html'):
            render_to_string('index.html')